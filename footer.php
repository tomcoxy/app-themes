<?php use App\Config\Registry; ?>
<footer id="footer">
  <div class="row container">
    <div id="footer_widget1" class="col-md-4">
      <div class="footer-widget first-footer-widget-area grid_4"><?php dynamic_sidebar('first-footer-widget-area'); ?></div>
    </div>
    <div id="footer_widget2" class="col-md-4">
      <div class="footer-widget second-footer-widget-area grid_4"><?php dynamic_sidebar('second-footer-widget-area'); ?></div>
    </div>
    <div id="footer_widget3" class="col-md-4">
      <div class="footer-widget third-footer-widget-area grid_4"><?php dynamic_sidebar('third-footer-widget-area'); ?></div>
    </div>
</div>
<div id="footer_legal">
<div class="container"><?php
echo  "&copy; " . date("Y") ." | " . get_bloginfo ('name' ) ." | " . __('All rights reserved. Designated trademarks and brands are the property of their respective owners.',Registry::TextDomain);
?></div>
</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>