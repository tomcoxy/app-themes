<?php
use App\Theme\FrontPage;
get_header();
?>
<div class="container">
	<h1>Hello, world!</h1>
	<p>Bootstrap Enabled. Check: <div class="spinner-grow" style="width: 3rem; height: 3rem;" role="status">
  <span class="sr-only">Loading...</span>
</div></p>
	<p>Font Awesome Enabled. Check: <i class="fas fa-clipboard-check fa-2x"></i></p>
</div>
<?php
get_footer();
?>