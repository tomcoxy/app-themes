App Themes
===

Hi. I'm a starter theme called `App Themes`. I offer a basic starting point for any developer that prefers writing in OOP rather than procedural. 

However I give flexibility by not altering the WordPress templating system and abiding by all the convensions off WordPress. So I give you an
App/ directory which can be used for business logic and called upon by using the relevant namespacing.

What's required?
---------------
A fresh approach I allow you to go to the cutting edge but not quite the bleeding edge so I will expect you to have the following:

- Latest version of WordPress Installed and Ready to Roll
- PHP > 5.6 (7+ preferred)


Getting Started
---------------
- Download the zip
- Upload the theme in WordPress Theme Area
- Happy coding!