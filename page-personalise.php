<?php 
/**
* Template Name: Personalise
*
*/
get_header();?>
<?php
	$card_id = get_post_meta( $_POST['cardid']);
	$uwpCardIDFront = $card_id['uwpCardIDFront'][0];
	$cardFront = getCardImages($uwpCardIDFront, $title);

?>
<header class="page-header">
<div class="page-title-image"  style="background-image: url('<?php echo $cardFront['main_url']; ?>')"></div>
<div class="imagemask"></div>
<?php theme_builder('breadcrumbs'); ?>
<div class="container"><?php the_title( '<h1 class="page-title">', '</h1>' ); ?></div>
</header><!-- .entry-header -->

<div id="cswrap" class="row">
	<div class="container">
    <div class="centcont col-md-8">
      <div id="cs">
      </div>
    </div>
    <div class="sidebar col-md-4">
    </div>
   </div>
</div>

<div id="page personalise" class="container">
	
<div class="row">
    <div class="card col-md-8">
    	<div id="ccWrap"></div>
      <div class="cardoverlay">
      	<div class="oContent">
        	<div class="icon"><i class="fa fa-circle-o-notch fa-spin" style="font-size:36px"></i></div>
        	<div class="title">Loading</div>
        	<div class="message">Please wait..</div>
        </div>
      </div>
    </div>
    
    <div class="sidebar col-md-4">

<?php get_template_part( 'template-parts/editortoolbar' ); ?>      
      
    </div>
    <!-- .summary --> 
  </div>
</div>
<?php get_footer(); ?>