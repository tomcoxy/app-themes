<?php
namespace App\Theme;
use App\Config\Registry;

class FrontPage {

	public function __construct()
	{
		add_action('init',[$this,'Init']);
		add_action( 'after_setup_theme',[$this,'ThemeSupport']);
		add_action( 'wp_enqueue_scripts', [$this, 'Scripts'] );
		add_action( 'widgets_init', [$this, 'Widgets'] );
	}

	public function Init()
	{

	}
	
	public function ThemeSupport()
	{
	
	}

	public function Scripts()
	{
		
		if (!is_admin() && is_front_page()) { 
			// Add anything here for the homepage only

		}
	
	}

	public function Widgets() 
	{

	}

}


?>