<?php
namespace App\Theme;
use App\Config\Registry;

class Theme {
	
	const versionDate = '20190721';

	public function __construct() {
		add_action( 'wp_enqueue_scripts', [$this, 'Scripts'] );
		add_action( 'widgets_init', [$this, 'Widgets'] );
	}
	
	public function Scripts()
	{
			if (!is_admin()) {
					wp_enqueue_style('bootstrap-min', get_template_directory_uri() . '/vendor/twbs/bootstrap/dist/css/bootstrap.min.css');
					wp_enqueue_style( Registry::TextDomain . '-font-awesome', get_template_directory_uri() . '/css/fonts/fp-pro-5.10.0-11/css/all.min.css');

					wp_enqueue_script( Registry::TextDomain . '-navigation', get_template_directory_uri() . '/js/navigation.js', array(), self::versionDate, true );
					wp_enqueue_script( Registry::TextDomain . '-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), self::versionDate, true );
					wp_enqueue_style( Registry::TextDomain . '-theme-style', get_stylesheet_uri() );
					
					$template_name = wp_basename(get_page_template());
					
						wp_deregister_script('jquery');
						wp_register_script('jquery', get_bloginfo('template_url') . '/vendor/components/jquery/jquery.min.js', false, null, true);
						wp_enqueue_script('bootstrap', get_bloginfo('template_url') . '/vendor/twbs/bootstrap/dist/js/bootstrap.min.js', array('jquery'), null, true);
						wp_enqueue_script('popper', get_bloginfo('template_url') . '/js/plugins/popper.min.js', array('jquery'), null, true);
						wp_enqueue_script('modernizr', get_bloginfo('template_url') . '/js/libs/modernizr.js', array('jquery'), null, true);
						wp_enqueue_script('jquery');
	
			}	
	
		
			if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
				wp_enqueue_script( 'comment-reply' );
			}
		
	}
	
	public function Widgets() {
			register_sidebar(array(
				'name'          => __('Sidebar', Registry::TextDomain),
				'id'            => 'sidebar-primary',
				'before_widget' => '<section class="blog-widget widget %1$s %2$s"><div class="widget-inner">',
				'after_widget'  => '</div></section>',
				'before_title'  => '<h4 class="default-info">',
				'after_title'   => '</h4><br />',
			));
	}
	
}


?>