<?php
namespace App\Theme;
use App\Config\Registry;

/**
 * Setup
 **/
class Setup {

	public function __construct()
	{
		add_action( 'after_setup_theme', [$this, 'LoadTextDomain'] );
		add_action( 'after_setup_theme', [$this, 'ThemeSupport'] );
	}

	/**
	 * Loads the international POT file for template string __('string','textdomain-slug')
	 **/
	public function LoadTextDomain() {
		load_theme_textdomain( Registry::TextDomain, Registry::getAppLocaleDir() );
	
	}
	
	/**
	 * Loads the basic theme support for the theme
	 **/
	public function ThemeSupport()
	{
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );
		
		/**
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 **/
		add_theme_support( 'title-tag' );
		
		/**
		 * Enable support for Post Thumbnails on posts and pages.
		 **/
		add_theme_support( 'post-thumbnails' );		
		
		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', [
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		]);
	
		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add theme support for Responsive Videos.
		add_theme_support( 'jetpack-responsive-videos' );
	
		// Add Menu Support
		register_nav_menus([
			'main-menu' => esc_html__( 'Main Menu', Registry::TextDomain )
		]);
		
	}

}