<?php
/**
 * Registry (Statically Available)
 * Default class variables for App
 **/
namespace App\Config;

class Registry {
	const TextDomain = 'app-theme';
	
	public static function getAppDir() {
		return get_template_directory() . DIRECTORY_SEPARATOR . 'App';
	
	}
	
	public static function getAppLocaleDir() {
		return get_template_directory() . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'Locale';
	
	}
	
	public static function getThemeDir() {
		return get_template_directory();
		
	}
	
	public static function getThemeUri() {
		return get_template_directory_uri();
		
	}
	
}