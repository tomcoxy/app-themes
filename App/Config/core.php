<?php
/**
 * Core (Do not edit)
 * Initiates ClassLoader and General Theme App Setup
 *
 **/
namespace App\Config;

require  'autoloader.php';

$loader = new \App\Config\ClassLoader;
$loader->addNamespace('App', get_template_directory() . DIRECTORY_SEPARATOR . 'App');
$loader->register();

define('DS',DIRECTORY_SEPARATOR);

// Bootstrap the application
require 'bootstrap.php';