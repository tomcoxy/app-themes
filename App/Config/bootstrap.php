<?php
/**
 * Bootstrap File (Pulling it all together)
 */

// Extra stuff (Merge to core!)
require get_template_directory() . DS . 'inc' . DS . 'template-tags.php';
require get_template_directory() . DS . 'inc' . DS . 'extras.php';
require get_template_directory() . DS . 'inc' . DS . 'wp-bootstrap-navwalker.php';

// ACF 5 Custom Fields
// 1. customize ACF path
add_filter('acf/settings/path', 'my_acf_settings_path');

function my_acf_settings_path( $path ) {
    // update path
    $path = get_stylesheet_directory() . '/inc/advanced-custom-fields-pro-master/';
    // return
    return $path;
}

// 2. customize ACF dir
add_filter('acf/settings/dir', 'my_acf_settings_dir');
 
function my_acf_settings_dir( $dir ) {
    // update path
    $dir = get_stylesheet_directory_uri() . '/inc/advanced-custom-fields-pro-master/';
    // return
    return $dir;
}
 
// 3. Hide ACF field group menu item
//add_filter('acf/settings/show_admin', '__return_false');
require get_template_directory() . DS . 'inc' . DS . 'advanced-custom-fields-pro-master' . DS . 'acf.php';

// 4. GitHub Updater Beta
require get_template_directory() . DS . 'inc' . DS . 'github-updater' . DS . 'github-updater.php';

// Runs Setup for Theme
new \App\Theme\Setup;
new \App\Theme\Theme;

/**
 * Load Vendors here
 **/

/**
 * Theme Mixins below here
 **/
new \App\Theme\FrontPage;